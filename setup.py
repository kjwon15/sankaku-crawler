#!/usr/bin/env python

from distutils.core import setup

def readme():
    try:
        with open('README.txt') as f:
            return f.read()
    except IOError:
        return

setup(
    name='sankaku',
    version='0.1',
    py_modules=['sankaku',],
    license='BSD License',
    long_description=readme(),
)
